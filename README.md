Steps:

1. Import .sql file to your mysql database

2. Change mysql configurations on projects's setting.py

3. Install django 1.9.6.

4. Migrate DB

5. runserver at 8000 port

6. add a crontab to your machine: @daily curl -I http://127.0.0.1:8000/daily/ (For very first time, please open http://127.0.0.1:8000/daily manually on browser as cron is set to run at midnight daily)


