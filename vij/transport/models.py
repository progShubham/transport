# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Truck(models.Model):
    truck_number = models.CharField(max_length=10)
    insurance_expiry = models.DateTimeField()
    fitness_expiry = models.DateTimeField()
    pollution_expiry = models.DateTimeField()
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'truck'


class Notifications(models.Model):
    days = models.IntegerField()
    unread = models.IntegerField()
    truck = models.ForeignKey('Truck', models.DO_NOTHING)
    created_at = models.DateTimeField()
    type_expiry = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'notifications'
