from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.http import JsonResponse
from transport.models import *
# import bcrypt
import random
import time
from django.views.decorators.csrf import csrf_exempt



def dashboard(request):
    # notifications = Notifications.objects.filter(unread=1).select_related('truck')
    query = 'SELECT notifications.id,notifications.days,notifications.type_expiry, truck.truck_number from notifications INNER JOIN truck ON truck.id = notifications.truck_id where notifications.unread = 1'
    notifications = Notifications.objects.raw(query)
    data = []
    for r in notifications:
        data.append({'truck_number':r.truck_number,'notification_id':r.id, 'days':str(r.days).replace('7','this week').replace('14','two weeks').replace('30','this month'),'type_expiry':str(r.type_expiry).replace('0','insurance certificate').replace('1','fitness certificate').replace('2','pollution certificate')})
    return render_to_response('dashboard.html', {'data':data})



@csrf_exempt
def notificationUpdate(request):
    print 'here'
    if request.method == 'POST':
        id = request.POST['id']
        print 'id ',id
        Notifications.objects.filter(id=id).update(unread=0)


# Generate random dates between two date
def strTimeProp(start, end, format, prop):
    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))
    ptime = stime + prop * (etime - stime)
    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%Y-%m-%d %H:%M:%S', prop)

def install_trucks(request):
    for r in range(1,1000):
        bwDate = randomDate("2018-01-01 12:00:00", "2018-12-31 12:00:00", random.random())
        p = Truck(truck_number='DL-'+ str(r).zfill(4), insurance_expiry=randomDate("2018-01-01 12:00:00", "2018-12-31 12:00:00", random.random()), fitness_expiry=randomDate("2018-01-01 12:00:00", "2018-12-31 12:00:00", random.random()), pollution_expiry=bwDate)
        p.save()
    return HttpResponse('1000 Trucks has been installed.')


def daily(request):
    try:
        query = 'SELECT id from truck where insurance_expiry <= DATE(NOW()) + INTERVAL 7 DAY and insurance_expiry >= DATE(NOW()) AND id NOT in (select truck_id from notifications where type_expiry = 0);'
        insurance = Truck.objects.raw(query)
        for i in insurance:
            p = Notifications(days=7,unread=1,truck_id=i.id,type_expiry=0)
            p.save()
        query = 'SELECT id from truck where insurance_expiry <= DATE(NOW()) + INTERVAL 14 DAY and insurance_expiry >= DATE(NOW()) + INTERVAL 7 DAY AND id NOT in (select truck_id from notifications where type_expiry = 0);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=14, unread=1, truck_id=i.id, type_expiry=0)
            p.save()
        query = 'SELECT id from truck where insurance_expiry <= DATE(NOW()) + INTERVAL 30 DAY and insurance_expiry >= DATE(NOW()) + INTERVAL 14 DAY AND id NOT in (select truck_id from notifications where type_expiry = 0);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=30, unread=1, truck_id=i.id, type_expiry=0)
            p.save()


        query = 'SELECT id from truck where fitness_expiry <= DATE(NOW()) + INTERVAL 7 DAY and fitness_expiry >= DATE(NOW()) AND id NOT in (select truck_id from notifications where type_expiry = 1);'
        insurance = Truck.objects.raw(query)
        for i in insurance:
            p = Notifications(days=7, unread=1, truck_id=i.id, type_expiry=1)
            p.save()
        query = 'SELECT id from truck where fitness_expiry <= DATE(NOW()) + INTERVAL 14 DAY and fitness_expiry >= DATE(NOW()) + INTERVAL 7 DAY AND id NOT in (select truck_id from notifications where type_expiry = 1);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=14, unread=1, truck_id=i.id, type_expiry=1)
            p.save()
        query = 'SELECT id from truck where fitness_expiry <= DATE(NOW()) + INTERVAL 30 DAY and fitness_expiry >= DATE(NOW()) + INTERVAL 14 DAY AND id NOT in (select truck_id from notifications where type_expiry = 1);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=30, unread=1, truck_id=i.id, type_expiry=1)
            p.save()


        query = 'SELECT id from truck where pollution_expiry <= DATE(NOW()) + INTERVAL 7 DAY and pollution_expiry >= DATE(NOW()) AND id NOT in (select truck_id from notifications where type_expiry = 2);'
        insurance = Truck.objects.raw(query)
        for i in insurance:
            p = Notifications(days=7, unread=1, truck_id=i.id, type_expiry=2)
            p.save()
        query = 'SELECT id from truck where pollution_expiry <= DATE(NOW()) + INTERVAL 14 DAY and pollution_expiry >= DATE(NOW()) + INTERVAL 7 DAY AND id NOT in (select truck_id from notifications where type_expiry = 2);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=14, unread=1, truck_id=i.id, type_expiry=2)
            p.save()
        query = 'SELECT id from truck where pollution_expiry <= DATE(NOW()) + INTERVAL 30 DAY and pollution_expiry >= DATE(NOW()) + INTERVAL 14 DAY AND id NOT in (select truck_id from notifications where type_expiry = 2);'
        insurance1 = Truck.objects.raw(query)
        for i in insurance1:
            p = Notifications(days=30, unread=1, truck_id=i.id, type_expiry=2)
            p.save()
    except Exception:
        return HttpResponse('No Data Available')
    return JsonResponse({'success': False, 'status': 3000,'data':'success'})


# Notifications
# +-------------+------------+------+-----+-------------------+----------------+
# | Field       | Type       | Null | Key | Default           | Extra          |
# +-------------+------------+------+-----+-------------------+----------------+
# | id          | int(11)    | NO   | PRI | NULL              | auto_increment |
# | days        | int(11)    | NO   |     | NULL              |                |
# | unread      | tinyint(4) | NO   |     | 1                 |                |
# | truck_id    | int(11)    | NO   |     | NULL              |                |
# | created_at  | timestamp  | NO   |     | CURRENT_TIMESTAMP |                |
# | type_expiry | int(11)    | NO   |     | NULL              |                |
# +-------------+------------+------+-----+-------------------+----------------+

# Truck
# +------------------+--------------------------+------+-----+-------------------+-----------------------------+
# | Field            | Type                     | Null | Key | Default           | Extra                       |
# +------------------+--------------------------+------+-----+-------------------+-----------------------------+
# | id               | int(6) unsigned zerofill | NO   | PRI | NULL              | auto_increment              |
# | truck_number     | varchar(10)              | NO   |     | NULL              |                             |
# | insurance_expiry | datetime                 | NO   |     | NULL              |                             |
# | fitness_expiry   | datetime                 | NO   |     | NULL              |                             |
# | pollution_expiry | datetime                 | NO   |     | NULL              |                             |
# | created_at       | timestamp                | NO   |     | CURRENT_TIMESTAMP | on update CURRENT_TIMESTAMP |
# +------------------+--------------------------+------+-----+-------------------+-----------------------------+



