from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^dashboard/', views.dashboard, name='dashboard'),
    url(r'^install/', views.install_trucks, name='install_trucks'),
    url(r'^daily/', views.daily, name='daily'),
    url(r'^notificationUpdate/', views.notificationUpdate, name='notificationUpdate'),
        ]
